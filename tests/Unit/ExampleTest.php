<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{

    private $token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI5YjFjYTk1M2QzNDViOTcxMGY4YWYxYTMwY2FkMGVmNDQwOGMyZDk3NmUxYjhkNmY0YjMxNGFlYWE3YmI4OWU5MGY2MDZjOWJlMWE4ZTU1In0.eyJhdWQiOiIzIiwianRpIjoiYjliMWNhOTUzZDM0NWI5NzEwZjhhZjFhMzBjYWQwZWY0NDA4YzJkOTc2ZTFiOGQ2ZjRiMzE0YWVhYTdiYjg5ZTkwZjYwNmM5YmUxYThlNTUiLCJpYXQiOjE1MTczNjQ4OTksIm5iZiI6MTUxNzM2NDg5OSwiZXhwIjoxNTQ4OTAwODk5LCJzdWIiOiIiLCJzY29wZXMiOlsidGVzdC1zeXN0ZW1zIl19.L_5dP_OuG9glqYMMSjWv3AjrAnYA9PD8qiSl2-0Jav-eAYPKswqOtHNEd4oSSg-wtqDSKpG4a7J4mhwsHTvU7XdkbR0xpyKoDPTR7faP0bjsT_EyfNpeFO8EClhhnD1EPq9AOUnAEo8zDzs391bHCOC-p8DlHPscmYlJMogAncJlhOxakDctOk8v4Fn3yWNm5H8D9Qd8dtN-4E2iRFHegXSD20-E0utyJiljjgNNFiFHP6kO3rOSCh8iWSYlUYuB-HjZFyxIBpZEUpxq6a4wAbAB7NQNQDzxU1RiCITkc42T75WKzVhI90WWcFGXTt2f4gPPZX74sklb--Y2HF_HsmI7f2E4zI22uOCd-POLV1FGpLeaOm7y2YRNeGbGvV_J79TI7VkHMuXTfHUhc-iy2cxaRPahAG1big7KnYnduMPBYGIkpwGJlrOwQiZvEQ7Hats1plUM9WY3QVwxii0gSjRHIk-jFxxONXYToYaLjXR8zEjpmF8XrmCQxqkRZx1LfxspZiX8_jEcVBxz6iQAW7zE8nI8uKlB9fHjZBh8yjbueAPzUvgkmGP2qam_Lp4G_mGaEenmeHQVHVJl9cJPVfNgSX7NJWBV2tS40u47c5G2BrYda3mbncnMfgB2aB1pJuIQNj7lvuWqVn_4S57smHf2XX7M3i_w5TeZ-YQvVhA";
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }


    public function test_talk_to_payment_processing(){
        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'http://heartcupstripe:8001/api/communication/check', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization'     => $this->token
            ]
        ]);
        $this->assertEquals('valid', $res->getBody());
    }




}
